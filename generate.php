<?php
$generated="[生成に失敗しました]";
$chaos_c=array("が","と","を","の","に","");
try{
        $recaptcha_token=$_POST['g-recaptcha-response'];
        $recaptcha_result=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcYl78UAAAAALrDjW8g4WiEgDBuk5lzPm4P-jK7&response=".$recaptcha_token);
        $recaptcha_json=json_decode($recaptcha_result);
	if($recaptcha_json->success==false){
		header("Location: index.php?error=Recaptcha(私はロボットではありません)にチェックを入れてください。");
                exit();
        }
	$pdo=new PDO('mysql:dbname=sakubungame;host=localhost',"sakubungame_s","");
	$when_s=$pdo->query("SELECT * FROM words WHERE type=0 ORDER BY RAND() LIMIT 0,1");
	$when_q=$when_s->fetch(PDO::FETCH_ASSOC);
	$where_s=$pdo->query("SELECT * FROM words WHERE type=1 ORDER BY RAND() LIMIT 0,1");
	$where_q=$where_s->fetch(PDO::FETCH_ASSOC);
	$who_s=$pdo->query("SELECT * FROM words WHERE type=2 ORDER BY RAND() LIMIT 0,1");
	$who_q=$who_s->fetch(PDO::FETCH_ASSOC);
	$withwho_s=$pdo->query("SELECT * FROM words WHERE type=2 ORDER BY RAND() LIMIT 0,1");
	$withwho_q=$withwho_s->fetch(PDO::FETCH_ASSOC);
	$what_s=$pdo->query("SELECT * FROM words WHERE type=3 ORDER BY RAND() LIMIT 0,1");
	$what_q=$what_s->fetch(PDO::FETCH_ASSOC);
	$how_s=$pdo->query("SELECT * FROM words WHERE type=4 ORDER BY RAND() LIMIT 0,1");
	$how_q=$how_s->fetch(PDO::FETCH_ASSOC);
	if($_POST['mode']=="russia"){
                $generated=$where_q['word']."では、".$what_q['word']."が".$who_q['word']."に".$how_q['word']."!!";
        }else if($_POST['mode']=="chaos"){
		$word_count=0;
		$generated="";
                while($word_count<rand(10,100)){
			$cq=$pdo->query("SELECT * FROM words ORDER BY RAND() LIMIT 0,1");
                        $cf=$cq->fetch(PDO::FETCH_ASSOC);
                        $generated=$generated.$cf['word'].$chaos_c[array_rand($chaos_c)];
                        $word_count++;
                }
        }else{
                $generated=$when_q['word']." ".$where_q['word']."で".$who_q['word']."と".$withwho_q['word']."が".$what_q['word']."を".$how_q['word'];
        }
}catch(Exception $e){
	header("Location: index.php?error=エラーが発生しました");
	exit();
}
?>
<html>
        <head>
                <meta name="robots" content="none">
		<meta http-equiv="content-type" charset="utf-8">
		<title>生成結果</title>
		<meta name="viewport" content="width=device-width,initial-scale=1">
        </head>
	<body>
		<h1>生成結果</h1>
		<?php print(htmlspecialchars($generated)); ?>
	</body>
</html>

