<html>
	<head>
		<meta name="robots" content="none">
		<meta http-equiv="content-type" charset="utf-8">
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<title>作文ゲーム(おそらくn番煎じ)</title>
		<meta name="viewport" content="width=device-width,initial-scale=1">
	</head>
	<body>
		<span style="color:#FF0000;">
<?php
	if(isset($_GET['error'])){
		print(htmlspecialchars($_GET['error'])."<br><hr><br>");
	}
?>
		</span>
		<h1>作文ゲーム(おそらくn番煎じ)</h1>
		<h2>登録</h2>
		<form action="regist.php" method="post">
			<input type="text" name="word" required>
			<br>
			種類:
			<select name="type">
				<option value="0">いつ</option>
				<option value="1">どこで</option>
				<option value="2">だれか/だれと</option>
				<option value="3">何を</option>
				<option value="4">どうした</option>
				<option value="5">その他(カオスモードのみで使用)</option>
			</select>
			<br>
			<div class="g-recaptcha" data-sitekey="6LcYl78UAAAAAIbhXoYZf-tzVRmoL6iL8hDVG2pU
"></div>
			<br>
			<button type="submit">登録</button>
		</form>
		<br>
		<hr>
		<h2>生成</h2>
		<form action="generate.php" method="post">
			<select name="mode">
				<option value="normal">通常</option>
				<option value="russia">ロシア的倒置法</option>
				<option value="chaos">カオス</option>
			</select>モード
			<br>
			<div class="g-recaptcha" data-sitekey="6LcYl78UAAAAAIbhXoYZf-tzVRmoL6iL8hDVG2pU
"></div>
			<br>
			<button type="submit">生成</button>
		</form>
		<br>
<hr>
                <h2>説明</h2>
                <br>
                <h3>通常モード</h3>
                <br>
                いつ　どこでだれとだれが何をどうした
                <br>
                の形式で出力されます。
                <br>
                <h3>ロシア的倒置法モード</h3>
                <br>
                (どこ)では、(何)が(だれ）を(どうした)!!
                <br>
                の形式で出力されます。
                <br>
                ソビエトロシアでは、文があなたを作る!!
                <h3>カオスモード</h3>
                <br>
                とにかくわけのわからないことになります。とりあえずやっ
てみてください。
	</body>
</html>

